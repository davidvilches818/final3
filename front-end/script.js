
document.getElementById('crearForm').addEventListener('submit', function (event) {
  event.preventDefault();
  crearProducto();
});


function crearProducto() {
  
  var nombre = document.getElementById('nombre').value;
  var precio = parseFloat(document.getElementById('precio').value);
  var cantidad = parseInt(document.getElementById('cantidad').value);

  
  var producto = {
    nombre: nombre,
    precio: precio,
    cantidad: cantidad
  };

  fetch('http://172.28.141.251:32600/api/productos/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(producto)
  })
    .then(function (response) {
      if (response.ok) {
        
        alert('Producto creado exitosamente');
       
        window.location.href = 'index.html';
      } else {
        
        alert('Error al crear el producto');
      }
    })
    .catch(function (error) {
      console.log('Error:', error);
      alert('Error al crear el producto');
    });
}


function listarProductos() {
  fetch('http://172.28.141.251:32600/api/productos/')
    .then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Error al obtener los productos');
      }
    })
    .then(function (data) {
      var productosBody = document.getElementById('productosBody');
      productosBody.innerHTML = '';

      data.forEach(function (producto) {
        var row = document.createElement('tr');
        row.innerHTML = '<td>' + producto.id + '</td>' +
          '<td>' + producto.nombre + '</td>' +
          '<td>' + producto.precio + '</td>' +
          '<td>' + producto.cantidad + '</td>';
        productosBody.appendChild(row);
      });
    })
    .catch(function (error) {
      console.log('Error:', error);
      alert('Error al obtener los productos');
    });
}


function listarProducto() {
  var productId = document.getElementById('productId').value;

  fetch('http://172.28.141.251:32600/api/productos/' + productId)
    .then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Error al obtener el producto');
      }
    })
    .then(function (data) {
      var productoData = document.getElementById('productoData');
      productoData.innerHTML = ''; 

      
      var row = document.createElement('tr');
      row.innerHTML = '<td>' + data.id + '</td>' +
        '<td>' + data.nombre + '</td>' +
        '<td>' + data.precio + '</td>' +
        '<td>' + data.cantidad + '</td>';
      productoData.appendChild(row);
    })
    .catch(function (error) {
      console.log('Error:', error);
      alert('Error al obtener el producto');
    });
}


function borrarProducto() {
  var id = document.getElementById('idInput').value;

  
  fetch('http://172.28.141.251:32600/api/productos/' + id, {
    method: 'DELETE'
  })
    .then(function (response) {
      if (response.ok) {
        
        alert('Producto borrado exitosamente');
        
        window.location.href = 'index.html';
      } else {
       
        alert('Error al borrar el producto');
      }
    })
    .catch(function (error) {
      console.log('Error:', error);
      alert('Error al borrar el producto');
    });
}
